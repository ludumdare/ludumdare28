﻿using UnityEngine;
using System.Collections;

public class BootstrapController : MonoBehaviour {

	void OnEnable() {
		
		D.Log("[BootstrapController] OnEnable");

		BootstrapStart.Start();
		BootstrapGame.Start();
		BootstrapPlayer.Start();
		BootstrapCloud.Start();
		BootstrapGraphics.Start();
		BootstrapEnd.Start();
		
	}
		
}

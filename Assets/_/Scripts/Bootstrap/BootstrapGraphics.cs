﻿
//	Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public static class BootstrapGraphics {

	public static void Start() {
		
		D.Log("[BootstrapGraphics] Start");	
		
		
		D.Trace(string.Format("- current screen resolution is {0} x {1}", Screen.currentResolution.width, Screen.currentResolution.height));
		D.Trace(string.Format("- current game resolution is {0} x {1}", Screen.width, Screen.height));
		
#if UNITY_EDITOR
		D.Trace(string.Format("- game is running in editor", Screen.width, Screen.height));
#endif
		
#if !UNITY_EDITOR
		
		if (GameManager.Instance.GameSettings.WindowFullScreen) {
			D.Log("- switching game to FullScreen mode");
			Screen.SetResolution(1980,1050,true);
		}
		
#endif
		
	}
}

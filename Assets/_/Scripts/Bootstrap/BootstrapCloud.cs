﻿using UnityEngine;
using System.Collections;

public static class BootstrapCloud {

	public static void Start() {
		D.Trace(LogCategory.System, "[BootstrapCloud] Start");

#if !UNITY_EDITOR		
		SDNScoreoid.Instance.UpdateUsage();
#endif
		
	}
}

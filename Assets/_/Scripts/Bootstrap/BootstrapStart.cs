﻿
//	Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using Simple;

public static class BootstrapStart {

	public static void Start() {
		
		Debug.LogWarning("[BootstrapStart] Start");	
		
		//	init logger
		
		D.Log(GameManager.Instance.GameSettings.GameName);
		D.Log(GameManager.Instance.GameSettings.GameCopyright);
		
		//	init simple console commands
		
		D.Log(LogCategory.System, "- creating simple console commands");
		
		SimpleConsole.AddCommand("hello", "Shows Hello World", __hello);
		SimpleConsole.AddCommand("sw", "Show Watches", __showWatches);
		SimpleConsole.AddCommand("hw", "Hide Watches", __hideWatches);
		SimpleConsole.AddCommand("play", "Play Level", __playLevel);
		SimpleConsole.AddCommand("weapon", "Set Weapon", __setWeapon);
	}
	
	private static string __hello(SimpleContainer args) {
		return "Hello World";	
	}

	private static string __showWatches(SimpleContainer args) {
		DLogger.Instance.m_showWatches = true;
		return "";
	}

	private static string __hideWatches(SimpleContainer args) {
		DLogger.Instance.m_showWatches = false;
		return "";
	}
	
	private static string  __playLevel(SimpleContainer args) {
		int level = args.getAtIndex(0).iData;
		GameManager.Instance.CurrentLevel = level;
		Application.LoadLevel("Game");
		return "Starting level " + level;
	}
	
	private static string __setWeapon(SimpleContainer args) {
		string w = args.getAtIndex(0).sData;
		GameManager.Instance.PlayerController.SetWeapon(w);
		return "Weapon set to " + w;
	}


}

﻿
//	Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public static class BootstrapEnd {

	public static void Start() {
		
		D.Trace(LogCategory.System, "[BootstrapEnd] Start");
		
		//	goto the next scene
		GameManager.Instance.NextScene = GameManager.Instance.GameSettings.GameStartingScene;
		GameManager.Instance.GotoNextScene();
		
	}
}

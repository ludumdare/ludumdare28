﻿using UnityEngine;
using System.Collections;

public static class BootstrapGame {

	public static void Start() {
		
		D.Log("[BootstrapGame] Start");
		GameManager.Instance.Init();
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SDNScoreoid : MonoSingleton<SDNScoreoid> {

	
	//	===	PUBLIC
	
	public void UpdateUsage() {
		
		D.Trace("[SDNScoreoid] AddUsageCount");
		
		D.Detail("- creating http package to increase game played count");
		
		string _data = string.Format("api_key={0}&key={1}&response=json", GameManager.Instance.GameSettings.ScoreoidApi, "usage");
		string _encr = Crypto.DES_Encrypt(_data,GameManager.Instance.GameSettings.ScoreoidKey);
		D.Detail(string.Format("- encrypted value is {0}", _encr));
		
		WWWForm form = new WWWForm();
		form.AddField("game_id", GameManager.Instance.GameSettings.ScoreoidGame);
		form.AddField("s", _encr);
		WWW www = new WWW( GameManager.Instance.GameSettings.ScoreoidUrl + "/api/getGameData", form);
		
		HTTP.Instance.HTTP_Request(www, __getUsage);	
		
	}
	
	//	===	PRIVATE
	
	private void __getUsage(WWW www) {
		
		D.Fine("[LevelManager] __loadLevelResponse");
		
		if (www.error == null) {
			
			D.Log(www.text);
			
			string _unencr = Crypto.DES_Decrypt(www.text, GameManager.Instance.GameSettings.ScoreoidKey);
			D.Log(_unencr);
			
			int usage = 0;
			
			Dictionary<string, object> data = MiniJSON.Json.Deserialize(_unencr) as Dictionary<string, object>;
			
			if (data.ContainsKey("usage")) {
				string _u = (string)data["usage"];
				usage = int.Parse(_u);
			}
			
			usage += 1;
			
			D.Log("- new usage count is {0}", usage);
			
			__setUsage(usage);
			
		} else {
			D.Error(www.error);
		}
	}
	
	private void __setUsage(int usage) {
		
		D.Fine("[LevelManager] __setUsage");
		
		string _data = string.Format("api_key={0}&key={1}&value={2}&response=json", GameManager.Instance.GameSettings.ScoreoidApi, "usage", usage);
		string _encr = Crypto.DES_Encrypt(_data,GameManager.Instance.GameSettings.ScoreoidKey);
		D.Detail(string.Format("- encrypted value is {0}", _encr));
		
		WWWForm form = new WWWForm();
		form.AddField("game_id", GameManager.Instance.GameSettings.ScoreoidGame);
		form.AddField("s", _encr);
		WWW www = new WWW( GameManager.Instance.GameSettings.ScoreoidUrl + "/api/setGameData", form);
		
		HTTP.Instance.HTTP_Request(www, __setUsageReponse);	
	}
	
	private void __setUsageReponse(WWW www) {
				
		D.Fine("[LevelManager] __setUsageResponse");
		D.Log(www.text);
	}
	
	//	===	MONO
	
	void OnEnable() {
	
	}
	
	
	
}

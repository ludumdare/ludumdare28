﻿
//	Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public class SDNPanel : MonoBehaviour {

	public bool			m_autoHide;
	public Vector2		m_showPosition;
	public Vector2		m_hidePosition;
	
	
	public void Hide() {
		D.Trace("[SDNPanel] Hide");
		__hide();
	}
	
	public void Show() {
		D.Trace("[SDNPanel] Show");
		__show();
	}
	
	void Start() {
		D.Trace("[SDNPanel] Start");
		if (m_autoHide) __hide();	
	}
	
	private Vector3 __newLocalPos(Vector2 vector) {
		D.Trace("[SDNPanel] __newLocalPos");
		return new Vector3(vector.x, vector.y, transform.localPosition.z);
	}
	

	private void __show() {
		D.Trace("[SDNPanel] __show");
		transform.localPosition = __newLocalPos(m_showPosition);
	}
	
	private void __hide() {
		D.Trace("[SDNPanel] __hide");
		transform.localPosition = __newLocalPos(m_hidePosition);
	}
	
	
}

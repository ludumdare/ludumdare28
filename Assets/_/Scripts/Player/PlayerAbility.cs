﻿using UnityEngine;
using System.Collections;

public class PlayerAbility : MonoBehaviour {
	
	public string			m_name;
	
	public bool				Activated	{ get; set; }
	
	private RayCaster		m_caster;
	private Ability			m_ability;
	
	

	//	PUBLIC 
	
	public void Fire() {
	
		D.Fine("[PlayerAbility] Fire");
		
		if (!Activated) return;
		
		D.Log("- activating ability {0}", m_ability.Name);
	}
	
	public void SetAbility(string name) {
	
		D.Trace("[PlayerAbility] SetAbility");
		m_ability = GameManager.Instance.GameAbilities.GetAbility(name);
		m_name = "none";
		if (m_ability != null) m_name = m_ability.Name;
	}
	
	//	PRIVATE
	
	//	MONO
	
	void OnEnable() {
		
		D.Trace("[PlayerWeapon] OnEnable");
	}
	
	void Start () {
	
		D.Trace("[PlayerWeapon] Start");
		m_caster = transform.GetComponent<RayCaster>();
	}
	
}

﻿using UnityEngine;
using System.Collections;

public class PlayerWeapon : MonoBehaviour {
	
	public string			m_name;
	
	public SkeletonAnimation	Skeleton	{ get; set; }
	
	private RayCaster			m_caster;
	private Weapon				m_weapon;
	private PlayerController	m_controller;
	
	public bool					m_reloading;
	
	
	//	PUBLIC
	
	public void Fire() {
		
		D.Fine("[PlayerWeapon] Fire");
		
		if (m_reloading) return;
		
		D.Log("- firing weapon {0}", m_weapon.Name);
		
		RaycastHit _hit = m_caster.Hit;
		
		D.Watch("PlayerWeapon", "attacking", "none");

		//	animate
		Skeleton.state.TimeScale = m_weapon.SpineTime;
		Skeleton.state.SetAnimation(0, m_weapon.Spine, false);
		//Skeleton.state.AddAnimation(0, "idle", true, 0);
		
		//	oooh, we hit something
		if (_hit.transform != null) {
			D.Watch("PlayerWeapon", "attacking", _hit.transform.tag);
			D.Log("- attacking {0}", _hit.transform.name);
			__attack(_hit);
		} 
		
		m_reloading = true;
		StartCoroutine(__reload());
	}
	
	public void SetWeapon(string name) {
	
		D.Trace("[PlayerWeapon] SetWeapon");
		m_weapon = GameManager.Instance.GameWeapons.GetWeapon(name);
		m_name = "none";
		if (m_weapon != null) {
			m_name = m_weapon.Name;
			m_caster.m_raySize = m_weapon.Range * 15;
			D.Watch("PlayerWeapon", "name", m_weapon.Name);
			D.Watch("PlayerWeapon", "range", m_weapon.Range);
			D.Watch("PlayerWeapon", "hit", m_weapon.Hit);
			D.Watch("PlayerWeapon", "damage", m_weapon.Damage);
			D.Watch("PlayerWeapon", "critical", m_weapon.Critical);
		}
	}
	
	//	PRIVATE
	
	private IEnumerator __reload() {
		
		D.Trace("[PlayerWeapon] __reload");
		yield return new WaitForSeconds(m_weapon.Reload);
		m_reloading = false;
	}
	
	private void __attack(RaycastHit hit) {
	
		D.Trace("[PlayerWeapon] __attack");
		
		if (hit.transform.tag == "Enemy") {
			//	nasty!!!
			EnemyController e = hit.transform.parent.transform.GetComponent<EnemyController>();
			
			if (e.m_hitpoints > 0) {
				
				if (e.WeaponAttack(m_weapon)) {
					//	spawn one shot effect
					GameObject _go = (GameObject)Instantiate(GameEffects.Instance.EffectHitText);
					_go.transform.position = new Vector3(hit.point.x, hit.point.y + 200, hit.point.z);
					
					//	play sound
					if (m_weapon.Sound == "punch") GameManager.Instance.GameSounds.PlayPunch();
					if (m_weapon.Sound == "kick") GameManager.Instance.GameSounds.PlayKick();
				}
			}			
		}
	}
	
	//	MONO
	
	void OnEnable() {
		
		D.Trace("[PlayerWeapon] OnEnable");
	}
	
	void Start () {
	
		D.Trace("[PlayerWeapon] Start");
		m_caster = transform.GetComponent<RayCaster>();
	}
	
}

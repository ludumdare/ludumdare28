﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	
	public float				m_speed;
	public bool					m_debug;
	public Vector2				m_range;
	
	public bool					Enabled 	{ get; set; }
	
	private GameManager			m_manager;
	private RayCaster			m_caster;
	private PlayerAbility		m_ability;
	private PlayerWeapon		m_weapon;
	private SkeletonAnimation	m_skeleton;
	
	private	Quaternion			m_left;
	private	Quaternion			m_right;
	
	private int					m_state;
	private int					m_lastState;
	
	//	PUBLIC
	
	public void SetWeapon(string name) {

		D.Trace("[PlayerController] SetWeapon");
		m_weapon.SetWeapon(name);
	}
	
	//	PRIVATE
	
	private void __update() {
		
		if (!Enabled) return;
		
		//	get dir based on player input
		Vector3 _dir = __inputDirection();
		
		//	get raycast
		RaycastHit _hit = m_caster.Hit;
		
		D.Watch("PlayerController", "hit tag", "none");
		
		//	if we hit something
		
		if (_hit.transform != null) {
			
			//	pushback
			D.Watch("PlayerController", "hit tag", _hit.transform.tag);
			//D.Watch("PlayerController", "hit normal", _hit.normal);
			Vector3 _hn = new Vector3(_hit.normal.x, 0, _hit.normal.z);
			//D.Watch("PlayerController", "pushback normal", _hit.normal);
			_dir += _hn * 1.5f;
			
			//	hit the enemy???
			if (_hit.transform.tag == "Enemy") {
				if (m_debug) Debug.DrawLine(transform.position, _hit.point, Color.yellow);
			}
		}
				

		D.Watch("PlayerController", "dir", _dir);
		
		//	move the player
		__move(_dir);
		
		//	fire weapon?
		__fire();
		
	}
	
	private void __lateUpdate() {
		
		D.Fine("[PlayerController] __lateUpdate");
		
		Vector3 _pos = transform.position;
		if (transform.position.z < 050) _pos.z = 050;
		if (transform.position.z > 685) _pos.z = 685;
		if (transform.position.x < m_range.x) _pos.x = m_range.x;
		if (transform.position.x > m_range.y) _pos.x = m_range.y;
		transform.position = _pos;
	}
	
	private Vector3 __inputDirection() {
		
		D.Fine("[PlayerController] __inputDirection");
		
		Vector3 _dir = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
		if (Input.GetAxis("Horizontal")>0) {
			D.Watch("PlayerController", "facing", "right");
			m_skeleton.transform.rotation = m_right;
			m_state = 1;
			m_manager.GameCamera.m_offset.x = -400;
		}
		if (Input.GetAxis("Horizontal")<0) {
			D.Watch("PlayerController", "facing", "left");
			m_skeleton.transform.rotation = m_left;
			m_state = 1;
			m_manager.GameCamera.m_offset.x = -800;
		}
		if (Input.GetAxis("Horizontal")==0) {
			D.Watch("PlayerController", "facing", "idle");
			m_state = 0;
		}
		if (Input.GetAxis("Vertical")!=0) {
			m_state = 1;
		}
		
		if (m_state != m_lastState) {
			
			if (m_state == 0) {
				m_skeleton.state.TimeScale = 1;
				m_skeleton.state.SetAnimation(0, "idle", true);
			}
			
			if (m_state == 1) {
				m_skeleton.state.TimeScale = 1;
				m_skeleton.state.SetAnimation(0, "walk", true);
			}
			
		}
		
		m_lastState = m_state;
		
		D.Watch("PlayerController", "state", m_state);
		
		return _dir;
	}
	
	private void __fire() {
		
		D.Fine("[PlayerController] __fire");
		
		if (Input.GetButtonDown("Fire1")) {
			m_weapon.Fire();
		}
		
		if (Input.GetButtonDown("Fire2")) {
			m_ability.Fire();
		}
	}
	
	private void __move(Vector3 dir) {
		
		D.Fine("[PlayerController] __move");
		
		transform.Translate(dir * m_speed * Time.deltaTime);
		D.Watch("PlayerController", "pos", transform.position);
	}
	
	//	MONO
	
	void OnEnable() {
		
		D.Trace("[PlayerController] OnEnable");
		m_manager = GameManager.Instance;
		m_manager.PlayerController = this;
	}
	
	void Start () {
	
		D.Trace("[PlayerController] Start");
		m_caster = transform.GetComponent<RayCaster>();
		m_weapon = transform.FindChild("Weapon").GetComponent<PlayerWeapon>();
		m_ability = transform.FindChild("Ability").GetComponent<PlayerAbility>();
		m_skeleton = transform.FindChild("Skeleton").GetComponent<SkeletonAnimation>();
		m_weapon.Skeleton = m_skeleton;
		m_left = transform.FindChild("Skeleton").transform.rotation;
		//m_right = Quaternion.Inverse(m_left);
		m_right = Quaternion.Euler(310,180,0);
		
		//	temporarily set weapon & ability
		
		m_weapon.SetWeapon("kick");
		
		m_skeleton.state.SetAnimation(0, "champ", false);
		
	}
	
	void Update () {
	
		D.Fine("[PlayerController] Update");
		__update();
	}
	
	void LateUpdate() {

		D.Fine("[PlayerController] LateUpdate");
		__lateUpdate();
	}
}

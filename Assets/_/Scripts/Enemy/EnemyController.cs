﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public float				m_speed;
	public int					m_defense;
	public int					m_attacks;
	public int					m_hit;
	public int					m_damage;
	public int					m_hitpoints;
	public int					m_critical;
	public int					m_reload;
	
	private bool				m_reloading;
	
	private RayCaster			m_caster;
	private SkeletonAnimation	m_skeleton;
	
	//	PUBLIC	
	
	public virtual void Attack() {
		
		D.Trace("[EnemyContoller] Attack");
		__attack();
	}
	
	public virtual void Move() {
	
		D.Fine("[EnemyContoller] Move");
		__move();
	}
	
	//	enemy is attacked by player weapon
	public bool WeaponAttack(Weapon weapon) {
		
		bool _return = false;
		
		D.Trace("[EnemyContoller] WeaponAttack");
		D.Log("- enemy attacked with weapon {0}", weapon.Name);
		//DestroyImmediate(gameObject);
		int tohit = weapon.Hit - m_defense;
		int hit = Random.Range(0,100);
		
		if (hit < tohit) {
			_return = true;
			m_hitpoints -= weapon.Damage;
		}
		
		if (m_hitpoints <=0) {
			m_speed = 0;
			StartCoroutine(__death());
		}
		
		return _return;
		
	}
	
	//	enemy is attacked by player ability
	public void AbilityAttack(Ability ability) {
		
		D.Trace("[EnemyContoller] AbilityAttack");
		D.Log("- enemy attacked with ability {0}", ability.Name);
	}
	
	//	PRIVATE
	
	private void __attack() {
		
		D.Trace("[EnemyController] __attack");
		m_reloading = true;
		StartCoroutine(__reload());
	}
	
	private void __lateUpdate() {
		
		D.Fine("[PlayerController] __lateUpdate");
		
		Vector3 _pos = transform.position;
		if (transform.position.z < 050) _pos.z = 050;
		if (transform.position.z > 685) _pos.z = 685;
		//if (transform.position.x < m_range.x) _pos.x = m_range.x;
		//if (transform.position.x > m_range.y) _pos.x = m_range.y;
		transform.position = _pos;
		
		if (transform.position.x < 0) DestroyImmediate(gameObject);
	}
	
	private void __move() {
		
		D.Fine("[EnemyController] __move");
	}
	
	private void __update() {
	
		D.Fine("[EnemyContoller] __update");
		
		//	always do movement
		
		Move();
		
		if (m_reloading) return;
		
		//	cast forward
		RaycastHit _hit = m_caster.Hit;
		
		D.Watch("EnemyController", "hit ag", "none");
		
		if (_hit.transform != null) {
			
			D.Watch("EnemyController", "hit tag", _hit.transform.tag);
		
			//	when it hits player attack
		
			if (_hit.transform.tag == "Player") {
				Attack();
			}
		}
	}
	
	private IEnumerator __reload() {
		
		D.Trace("[EnemyController] __reload");
		yield return new WaitForSeconds(m_reload);
		m_reloading = false;
	}
	
	private IEnumerator __death() {
		
		m_skeleton.state.SetAnimation(0, "death1", false);
		yield return new WaitForSeconds(1);
		DestroyImmediate(gameObject);
	}
	
	//	MONO
	
	void OnEnable() {
		
		m_skeleton = transform.FindChild("Skeleton").GetComponent<SkeletonAnimation>();
	}
	
	
	void Start () {
	
		D.Trace("[EnemyController] Start");
		m_caster = transform.GetComponent<RayCaster>();
	}
	
	void Update () {
	
		D.Fine("[EnemyController] Update");
		__update();
	}
	
	void LateUpdate() {
		D.Fine("[EnemyController] LateUpdate");
		__lateUpdate();	
	}
}

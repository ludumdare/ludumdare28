﻿using UnityEngine;
using System.Collections;

public class EnemyMover : EnemyController {

	public override void Move ()
	{
		float sp = (m_speed + GameManager.Instance.CurrentLevel) * 10;
		Vector3 dir = Vector3.left;
		transform.Translate(dir * (m_speed * 10) * Time.deltaTime);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {
	
	public float						m_delay;
	public List<EnemyController>		m_enemyList;
	public int							m_total;		// total enemies to destroy
	public Vector2						m_range;
	
	Dictionary<string, EnemyController>	m_enemies;

	private bool						m_spawn;
	private int							m_count;
	
	//	PUBLIC
	
	public void Init() {
		
		D.Trace("[EnemySpawner] Init");
		m_total = 10 + (GameManager.Instance.CurrentLevel * 5);
		//m_delay = 0.5f;
		m_count = 0;
	}
	
	public void Play() {
		
		D.Trace("[EnemySpawner] Play");
		m_spawn = true;
		
	}
	
	//	PRIVATE
	
	private IEnumerator __spawn() {
		
		D.Trace("[EnemySpawner] __spawn");
		yield return new WaitForSeconds(m_delay + Random.Range(0,(40-GameManager.Instance.CurrentLevel)*0.1f));
		Transform go =  (Transform)Instantiate(m_enemies["EnemyDummy"].transform, transform.position, Quaternion.identity);
		go.position = new Vector3(go.transform.position.x, go.transform.position.y, go.transform.position.z + Random.Range(-400,400));
		m_spawn = true;		
		m_count += 1;
	}
	
	//	MONO
	
	void OnEnable() {
		
		m_enemies = new Dictionary<string, EnemyController>();
		
		GameManager.Instance.EnemySpawner = this;
		foreach(EnemyController e in m_enemyList) {
			D.Log(e.name);
			m_enemies.Add(e.name, e);	
		}
	}
	
	void Update() {
		
		if (!m_spawn) return;
		
		if (m_count > m_total) return;
		
		m_spawn = false;
		StartCoroutine(__spawn());
		
	}
	
}

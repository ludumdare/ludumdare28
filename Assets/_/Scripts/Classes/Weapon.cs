﻿using UnityEngine;
using System.Collections;

public class Weapon {
	
	public string			Id			{ get; set; }	//	unique id -- assigned at runtime
	public string			Name		{ get; set; }	//	name of weapon -- used in lookup
	public int				Range		{ get; set; }	//	range of weapon -- used by raycaster
	public int				Attacks		{ get; set; }	//	number of attacks
	public int				Hit			{ get; set; }	//	chance to hit
	public int				Damage		{ get; set; }	//	damage per hit
	public int				Critical	{ get; set; }	//	chance for critical hit
	public float			Reload		{ get; set; }	//	reload delay
	public string			Spine		{ get; set; }	//	spine animation -- used by spine animator -- default == "attack"
	public float			SpineTime	{ get; set; }	//	animation speed
	public string			Sound		{ get; set; }	//	attack sound

	public Weapon() {
		
		D.Trace("[Weapon]");
		
		Id = System.Guid.NewGuid().ToString();
		Name = "unknown";
		Range = 10;
		Attacks = 1;
		Hit = 5;
		Damage = 1;
		Critical = 1;
		Spine = "attack";
		Reload = 1;
		SpineTime = 1;
		Sound = "punch";
	}
}

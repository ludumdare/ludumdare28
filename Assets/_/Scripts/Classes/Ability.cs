﻿using UnityEngine;
using System.Collections;

public class Ability {

	public string			Id			{ get; set; }
	public string			Name		{ get; set; }
	public int				Range		{ get; set; }
	public int				Hit			{ get; set; }
	public int				Damage		{ get; set; }
	public int				Critical	{ get; set; }
	public string			Spine		{ get; set; }
	
	public Ability() {
		
		D.Trace("[Ability]");
		
		Id = System.Guid.NewGuid().ToString();
		Name = "Unknown";
		Range = 10;
		Hit = 5;
		Damage = 1;
		Critical = 1;
		Spine = "attack";
	}
}

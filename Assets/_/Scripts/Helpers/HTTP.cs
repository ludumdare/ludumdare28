﻿using UnityEngine;
using System.Collections;

public class HTTP : MonoSingleton<HTTP> {
	
	public delegate void HTTP_RequestDelegate(WWW www);
	
	//private string m_url = "http://dev1.sdngames.com:5000";
	//private string m_url = "http://192.168.15.45:5000";
	//private string m_key = "40b34922";
	
	public string Key { get { return GameManager.Instance.GameSettings.ScoreoidKey; } }
	
	public WWW CreateRequest(string uri, string data) {
		
		D.Trace("[HTTP] CreateRequest(uri, data)");
		return __createRequest(GameManager.Instance.GameSettings.GameUrl, uri, data);
	}
	
	public WWW CreateRequest(string url, string uri, string data) {
		
		D.Trace("[HTTP] CreateRequest(uri, data)");
		return __createRequest(url, uri, data);
	}
	
	private WWW __createRequest(string url, string uri, string data) {
		D.Trace("[HTTP] __createRequest");
		WWWForm form = new WWWForm();
		form.AddField("data", data);
		WWW www = new WWW( url + uri, form);
		return www;
	}
	
	public void HTTP_Request(WWW www, HTTP_RequestDelegate del) {
		D.Trace("[HTTP] __createRequest");
		D.Log(LogCategory.Network, string.Format("making http request to {0}", www.url));
		StartCoroutine(HTTP_Response(www, del));
	}
	
	private IEnumerator HTTP_Response(WWW www, HTTP_RequestDelegate del) {
		yield return www;
		del(www);
		
	}
	
}

public enum HttpResponses {
	
	HTTP_OK,
	HTTP_ERROR,
	HTTP_BAD_REQUEST
}

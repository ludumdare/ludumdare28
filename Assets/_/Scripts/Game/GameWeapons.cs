﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameWeapons {
	
	public Dictionary<string, Weapon>		m_weapons;
	
	//	PUBLIC
	
	public Weapon GetWeapon(string name) {
	
		Weapon _return = null;
		
		if (m_weapons.ContainsKey(name)) {
			_return = m_weapons[name];
		} else {
			D.Warn("- weapon {0} is not available");
		}
		
		return _return;
	}
	
	//	PRIVATE
	
	
	public GameWeapons() {
		
		D.Trace("[GameWeapons]");
	
		m_weapons = new Dictionary<string, Weapon>();
		
		Weapon w;
		
		//	punch
		
		w = new Weapon();
		w.Name = "punch";
		w.Range = 5;
		w.Attacks = 1;
		w.Hit = 85;
		w.Damage = 1;
		w.Critical = 10;
		w.Spine = "punch";
		w.SpineTime = 5.0f;
		w.Reload = 0.20f;
		w.Sound = "punch";
		m_weapons.Add(w.Name, w);
		
		//	kick
		
		w = new Weapon();
		w.Name = "kick";
		w.Range = 10;
		w.Attacks = 1;
		w.Hit = 95;
		w.Damage = 2;
		w.Critical = 15;
		w.Spine = "kick";
		w.SpineTime = 5.0f;
		w.Reload = 0.20f;
		w.Sound = "kick";
		m_weapons.Add(w.Name, w);
		
		//	stomp
		
		w = new Weapon();
		w.Name = "stomp";
		w.Range = 20;
		w.Attacks = 1;
		w.Hit = 60;
		w.Damage = 5;
		w.Critical = 30;
		w.Spine = "stomp";
		w.Sound = "kick";
		m_weapons.Add(w.Name, w);
		
	}
	
}

﻿using UnityEngine;
using System.Collections;

public class GameEffects : MonoSingleton<GameEffects> {

	public GameObject		EffectHitGround;
	public GameObject		EffectSmoke;
	public GameObject		EffectHitText;
	public GameObject		EffectHitPoof;
	public GameObject		EffectExplosion;
	
}

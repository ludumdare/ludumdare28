﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameAbilities {

	private Dictionary<string, Ability>		m_abilities;
	
	//	PUBLIC
	
	public Ability GetAbility(string name) {
	
		Ability _return = null;
		
		if (m_abilities.ContainsKey(name)) {
			_return = m_abilities[name];
		} else {
			D.Warn("- weapon {0} is not available");
		}
		
		return _return;
	}
	
	//	PRIVATE
	
	
	public GameAbilities() {
		
		D.Trace("[GameAbilities]");
	
		m_abilities = new Dictionary<string, Ability>();
		
		Ability a;
		
		//	punch
		
		a = new Ability();
		a.Name = "roundhouse";
		a.Range = 5;
		a.Hit = 5;
		a.Damage = 1;
		a.Critical = 1;
		a.Spine = "roundhouse";
		m_abilities.Add(a.Name, a);
		
	}
}

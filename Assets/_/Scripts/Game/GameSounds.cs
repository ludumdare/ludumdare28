﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSounds : MonoBehaviour {
	
	public List<AudioClip>		m_punch;
	public List<AudioClip>		m_robots;
	public List<AudioClip>		m_yells;
	public List<AudioClip>		m_kick;
	public List<AudioClip>		m_explosions;
	public List<AudioClip>		m_gunMiss;
	public List<AudioClip>		m_meleeMiss;
	
	public AudioClip			m_gun;
	public AudioClip			m_shotgun;
	public AudioClip			m_machineGun;
	public AudioClip			m_rocketLauncher;
	public AudioClip			m_sparks;
	public AudioClip			m_startLevel;
	

	public void PlayPunch() {
	
		D.Trace("[GameSounds] PlayPunch");
		__playSoundOnce(__getRandomClip(m_punch));	
	}
	
	public void PlayKick() {
	
		D.Trace("[GameSounds] PlayKick");
		__playSoundOnce(__getRandomClip(m_kick));	
	}
	
	public void PlayStartLevel() {
	
		D.Trace("[GameSounds] PlayKick");
		__playSoundOnce(m_startLevel);		
	}
	
	private AudioClip __getRandomClip(List<AudioClip> list) {
				
		D.Trace("[GameSounds] __getRandomClip");
		int i = Random.Range(0, list.Count-1);
		D.Log(i);
		return list[i];
	}
	
	private void __playSoundOnce(AudioClip clip) {
		
		D.Trace("[GameSounds] __playSoundOnce");
		audio.PlayOneShot(clip);
	}
	
	void OnEnable() {
		
		D.Trace("[GameSounds] OnEnable");
		GameManager.Instance.GameSounds = this;
	}
}

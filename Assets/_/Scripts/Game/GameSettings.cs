﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSettings {

	public enum Mode {
		GAME_MODE_TEST	= 9,
		GAME_MODE_BETA	= 1,
		GAME_MODE_GA 	= 0
	}
	
	// game settings
	public Mode			GameMode			{ get; set; }
	public string		GameName			{ get; set; }
	public string		GameStartingScene	{ get; set; }
	public string		GameCopyright		{ get; set; }
	public string		GameWebsite			{ get; set; }
	public string 		GameVersion			{ get; set; }
	public string		GameSaveFilename	{ get; set; }
	public string		GameUrl				{ get; set; }
	
	// windows settings
	public int			WindowWidth			{ get; set; }
	public int			WindowHeight		{ get; set; }
	public bool			WindowFullScreen	{ get; set; }
	
	// audio settings
	public float		MusicVolume			{ get; set; }
	public float		SoundVolume			{ get; set; }
	
	//	scoreoid settings
	public string		ScoreoidUrl			{ get; set; }
	public string		ScoreoidApi			{ get; set; }
	public string		ScoreoidGame		{ get; set; }
	public string		ScoreoidKey			{ get; set; }
	
	public void Load() {
		
		D.Trace(LogCategory.System, "[GameSettings] Load");
		__loadRemote();
		__loadLocal();
	}
	
	private void __loadLocal() {
		
		D.Trace(LogCategory.System, "[GameSettings] __loadLocal");
		string json = SDNGames.LoadJsonResourceFile("game");
		Dictionary<string, object> _data = SDNGames.JsonDeserialize(json);	
		Dictionary<string, object> _settings = _data["settings"] as Dictionary<string, object>;
		__loadSettings(_settings);
	}
	
	private void __loadRemote() {
		
		D.Trace(LogCategory.System, "[GameSettings] __loadRemote");
	}
	
	private void __loadSettings(Dictionary<string, object> settings) {
		
		D.Trace(LogCategory.System, "[GameSettings] __loadSettings");
		
		GameMode 			= 
			(Mode)(int)(long) settings["gameMode"];
		
		GameName 			= (string)		settings["gameName"];
		GameStartingScene 	= (string)		settings["gameStartingScene"];
		GameCopyright 		= (string)		settings["gameCopyright"];
		GameWebsite 		= (string)		settings["gameWebsite"];
		GameVersion 		= (string)		settings["gameVersion"];
		GameSaveFilename	= (string)		settings["gameSaveFilename"];
		GameUrl				= (string)		settings["gameUrl"];
		
		WindowWidth 		= (int)(long)	settings["windowWidth"];
		WindowHeight 		= (int)(long)	settings["windowHeight"];
		
		WindowFullScreen	= 
			(int)(long) settings["windowFullScreen"] == 1;
		
		MusicVolume			= (float)(double)settings["musicVolume"];
		MusicVolume			= (float)(double)settings["soundVolume"];
		
		ScoreoidUrl			= (string)		settings["scoreoidUrl"];
		ScoreoidApi			= (string)		settings["scoreoidApi"];
		ScoreoidGame		= (string)		settings["scoreoidGame"];
		ScoreoidKey			= (string)		settings["scoreoidKey"];
	}
	
}

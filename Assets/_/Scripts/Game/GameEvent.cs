// Copyright 2012, SpockerDotNet LLC

// Thanks to Will Miller @ http://www.willrmiller.com for the Original Source.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * A Type-Safe event system for Unity.
 * 
 * This implementation is based upon the Event Listener class
 * introduced by Will Miller (http://www.willrmiller.com/?p=87)
 * 
 * Events act as an explicit interface between objects. With this
 * we can introduce loosely coupled event driven code.
 * 
 **/

/**
 * This GameEvent class should be the parent for all 
 * custom GameEvents in the game.
 * 
 **/

#region GameEvent
public class GameEvent
{ 
}
#endregion

/**
 * How to use this Class
 * 
 * To properly use this event class you must add and remove listeners from the GameEvents object.
 * 
 * 
 * 
 **/

#region GameEvents
public sealed class GameEvents : Singleton<GameEvents>
{
	
/*
    private static GameEvents eventsInstance = null;
    public static GameEvents instance
    {
        get
        {
            if (eventsInstance == null)
            {
                eventsInstance = new GameEvents();
            }
 
            return eventsInstance;
        }
    }

 */
	
	GameEvents() { }
	
    public delegate void EventDelegate<T> (T e) where T : GameEvent;
 
    private Dictionary<System.Type, System.Delegate> delegates = new Dictionary<System.Type, System.Delegate>();
 
    public void AddListener<T> (EventDelegate<T> del) where T : GameEvent
    {
        if (delegates.ContainsKey(typeof(T)))
        {
            System.Delegate tempDel = delegates[typeof(T)];
 
            delegates[typeof(T)] = System.Delegate.Combine(tempDel, del);
        }
        else
        {
            delegates[typeof(T)] = del;
        }
    }
 
    public void RemoveListener<T> (EventDelegate<T> del) where T : GameEvent
    {
        if (delegates.ContainsKey(typeof(T)))
        {
            var currentDel = System.Delegate.Remove(delegates[typeof(T)], del);
 
            if (currentDel == null)
            {
                delegates.Remove(typeof(T));
            }
            else
            {
                delegates[typeof(T)] = currentDel;
            }
        }
    }
 
    public void Raise (GameEvent e)
    {
		D.Log(LogCategory.System, string.Format("Raising Event [{0}]", e));
        if (e == null)
        {
            D.Log(LogCategory.System, "Invalid event argument: " + e.GetType().ToString());
            return;
        }
 
        if (delegates.ContainsKey(e.GetType()))
        {
            delegates[e.GetType()].DynamicInvoke(e);
        }
    }
}
#endregion
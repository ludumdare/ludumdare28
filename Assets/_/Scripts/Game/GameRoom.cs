﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameRoom : MonoBehaviour {

	public GameObject				m_prefabWall;
	public GameObject				m_prefabFloor;
	public GameObject				m_prefabDoor;
	public List<GameObject>			m_prefabBricks;
	public List<GameObject>			m_prefabTiles;
	public List<GameObject>			m_prefabWallDecos;
	public List<GameObject>			m_prefabWindows;
	public List<GameObject>			m_prefabFloorDecos;
	
	private tk2dTiledSprite			m_wall;
	private tk2dTiledSprite			m_floor;
	
	public void Build(int width) {
	
		D.Trace("[GameRoom] Build");
		
		GameObject world = new GameObject("@World");

		GameObject go;
		
		go = (GameObject)Instantiate(m_prefabWall);
		m_wall = go.transform.GetComponent<tk2dTiledSprite>();
		m_wall.dimensions = new Vector2(width, m_wall.dimensions.y);
		go.transform.parent = world.transform;
		
		go = (GameObject)Instantiate(m_prefabFloor);
		m_floor = go.transform.GetComponent<tk2dTiledSprite>();
		m_floor.dimensions = new Vector2(width, m_floor.dimensions.y);
		go.transform.parent = world.transform;
		
		//	create door
		
		go = (GameObject)Instantiate(m_prefabDoor);
		go.transform.position = new Vector3(width - 1024, 310, 890);
		go.transform.parent = world.transform;

		
		
		//	decorate wall
		
		for (int i = 0; i < width; i += 128) {
			
			int x = i;
			
			for (int j = 0; j < 5; j += 1) {
				
				int y = 240 + (j * 128);
				
				int p = Random.Range(0,2); 	// 1 = draw
				
				if (p == 1) {
				
					int f = Random.Range(0, m_prefabBricks.Count -1);
					GameObject gf = (GameObject)Instantiate(m_prefabBricks[f]);
					gf.transform.position = new Vector3(x, y, 910);
					gf.transform.parent = world.transform;
				}
			}
			
			
		}
		
		//	decorate floor
		
		for (int i = 0; i < width; i += 256) {
			
			for (int j = 0; j < 5; j += 1) {
				
				int x = i;
				int z = 700 - (j * 256);
				
				int t = Random.Range(0,3); 	// 1 = draw
				
				if (t == 1) {
				
					int f = Random.Range(0, m_prefabTiles.Count -1);
					GameObject gf = (GameObject)Instantiate(m_prefabTiles[f]);
					gf.transform.position = new Vector3(x,240,z);
					gf.transform.parent = world.transform;
				}
				
			}
			
			
		}
	}
	
	void OnEnable() {
		
		GameManager.Instance.GameRoom = this;
	}
}

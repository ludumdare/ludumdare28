﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {
	
	public Transform			m_target;
	public Vector3				m_offset;
	public Vector2				m_range;
	public float				m_speed;
	
	void OnEnable() {
		
		GameManager.Instance.GameCamera = this;
		
	}
	
	void Update () {
		
		if (m_target == null) return;

		Vector3 _pos = new Vector3(m_target.transform.position.x, transform.position.y, transform.position.z);
		
		_pos = _pos + m_offset;
		
		if (_pos.x < m_range.x) _pos.x = m_range.x;
		if (_pos.x > m_range.y) _pos.x = m_range.y;
		
		transform.position = Vector3.Lerp(transform.position, _pos, m_speed * Time.deltaTime);
	}
}

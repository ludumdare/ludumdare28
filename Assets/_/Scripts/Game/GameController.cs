using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameController : MonoBehaviour {
	
	private GameManager			m_manager;
	

	//	PUBLIC
	
	public void Play() {
		
		Vector3 _pos = new Vector3(m_manager.PlayerController.transform.position.x, m_manager.PlayerController.transform.position.y - 10000, m_manager.PlayerController.transform.position.z);
		HOTween.To(m_manager.PlayerController.transform, 0.5f, new TweenParms().Prop("position", _pos).OnComplete(__play));
		m_manager.GameSounds.PlayStartLevel();
	}
	
	
	
	//	PRIVATE
	
	private void __play() {
		
		//	break the ground
		Instantiate(GameEffects.Instance.EffectHitGround, m_manager.PlayerController.transform.position, Quaternion.identity);
		
		//	setup camera
		m_manager.GameCamera.m_target = m_manager.PlayerController.transform;
		
		//	enable player
		m_manager.PlayerController.Enabled = true;		
		
		//	start music
		//m_manager.GameMusic.PlayGameMusic();
		
		//	start spawner
		m_manager.EnemySpawner.Play();
	}
	
	//	MONO
	
	void OnEnable() {
		
		D.Trace("[GameController] OnEnable");
		m_manager = GameManager.Instance;
		m_manager.GameController = this;
	}
	
	void Start() {
	
		D.Trace("[GameController] Start");
		
		//	hide player
		m_manager.PlayerController.transform.position = new Vector3(m_manager.PlayerController.transform.position.x, m_manager.PlayerController.transform.position.y + 10000, m_manager.PlayerController.transform.position.z);
		
		//	set level props
		
		//	width = 2 starting + standard 2048 + 1024 every five levels + 1024 end zone
		int _levelWidth = (512 * 2) + 2048 + ( Mathf.CeilToInt(m_manager.CurrentLevel/5) * 1024) + 1024;
		
		//	build level
		GameManager.Instance.GameRoom.Build(_levelWidth);
		m_manager.PlayerController.m_range = new Vector2(0,_levelWidth - 128);
		
		//	set camera
		GameManager.Instance.GameCamera.m_target = null;
		GameManager.Instance.GameCamera.m_range = new Vector2(0, _levelWidth - 1400);
		
		//	setup enemy spawner
		m_manager.EnemySpawner.Init();
		
		//	setup drops
				
		//	show ready		
		GameObject.Find("ReadyPanel").GetComponent<ReadyPanel>().Show();
		
	}
}

﻿
//	Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameManager : MonoSingleton<GameManager> {

	//	PUBLIC
	
	public GameSettings			GameSettings		{ get; set; }
	public GameWeapons			GameWeapons			{ get; set; }
	public GameAbilities		GameAbilities		{ get; set; }
	public GameCamera			GameCamera			{ get; set; }
	public GameController		GameController		{ get; set; }
	public GameRoom				GameRoom			{ get; set; }
	public GameEffects			GameEffects			{ get; set; }
	public GameSounds			GameSounds			{ get; set; }
	public GameMusic			GameMusic			{ get; set; }
	public EnemySpawner			EnemySpawner		{ get; set; }
	public PlayerController		PlayerController	{ get; set; }
	public GameScore			GameScore			{ get; set; }
	public GameBestScore		GameBestScore		{ get; set; }
	
	public int					CurrentLevel		{ get; set; }
	
	public string				NextScene			{ get; set; }
	public string				LastScene			{ get; set; }
	
	public void Init() {
		D.Trace("[GameManager] Init");
		__init();	
	}
	
	public void Play() {
	
		GameController.Play();
	}
	
	public void GotoNextScene() {
		
		D.Trace("[GameManager] GotoNextScene");
		Application.LoadLevel(NextScene);
	}

	public void GotoLastScene() {
		
		D.Trace("[GameManager] GotoLastScene");
	}

	//	PRIVATE
	
	private void __init() {
		
		D.Trace("[GameManager] __init");
		CurrentLevel = 1;
	}
	
	//	MONO
	
	void OnEnable() {
		
		D.Trace("[GameManager] OnEnable");
		GameSettings = new GameSettings();
		GameAbilities = new GameAbilities();
		GameWeapons = new GameWeapons();
		GameSettings.Load();
		HOTween.Init();
	}
	
	void Start() {
			
		D.Trace("[GameManager] Start");
	}
}

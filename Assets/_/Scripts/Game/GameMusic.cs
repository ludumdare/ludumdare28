﻿using UnityEngine;
using System.Collections;

public class GameMusic : MonoBehaviour {
	
	public AudioClip				m_gameMusic;
	public AudioClip				m_introMusic;
	public AudioClip				m_endingMusic;
	public AudioClip				m_completeMusic;
	
	public void PlayGameMusic() {
		__playClip(m_gameMusic);
	}
	
	public void PlayIntroMusic() {
		__playClip(m_introMusic);	
	}
	
	public void PlayEndingMusic() {
		__playClip(m_endingMusic);	
	}
	
	public void PlayCompleteMusic() {
		__playClip(m_completeMusic);
	}
	
	private void __playClip(AudioClip clip) {
		
		audio.Stop();
		audio.loop = true;
		audio.clip = clip;
		audio.Play();
	}
	
	
	void OnEnable() {
		
		GameManager.Instance.GameMusic = this;
	}
}

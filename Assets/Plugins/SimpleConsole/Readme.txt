For comments, questions and bug reports, please contact Anthony Smith at asmith@anthonysjournal.info or at http://www.twitter.com/anthonytrilli

When contacting, please send the invoice number that you received when you purchased SimpleConsole.
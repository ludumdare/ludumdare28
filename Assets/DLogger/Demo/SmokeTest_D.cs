

using UnityEngine;
using System.Collections;

public class SmokeTest_D : MonoBehaviour {
	
	private int i;
	
	// Use this for initialization
	void Start () {
		
		D.Error("Test");
		D.Error("Test {0}", 1);
		D.Warn("Test");
		D.Warn("Test {0}", 1);
		D.Log("Test");
		D.Log("Test {0}", 1);
		D.Log(LogCategory.Game, "Test {0}", 2);
		D.Detail("Test");
		D.Detail("Test {0}", 1);
		D.Detail(LogCategory.Game, "Test {0}", 2);
		D.Trace("Test");
		D.Trace("Test {0}", 1);
		D.Trace(LogCategory.Game, "Test {0}", 2);
	}
	
	void Update() {
		i++;	
		D.Log("Another Log Entry");
		D.Log(LogCategory.Game, "SmokeTest i = {0}", i);
		D.Log(LogCategory.Camera, "Some GameObject = " + gameObject);
	}

}


// Portions are Copyright 2011-2013, SpockerDotNet LLC http://www.sdnllc.com/
// Version 0.1

/**
 * Orignial version of this Script can be found at:
 * 
 * 		https://github.com/prime31/P31UnityAddOns/blob/master/Scripts/Debugging/D.cs
 * 
 * 
 * 
 * We suggest that you use the Global Defines wizard from Prime31 to manage these
 * 
 * 		https://github.com/prime31/P31UnityAddOns/blob/master/Editor/GlobalDefinesWizard.cs
 * 
 * 
 * 
 * To turn on a feature, simply uncomment the #define you want to activate. 
 * To turn it off, simply recomment the line.
 * 
 * Note that a recompile is required before the affect will take place.
 * 
 **/

//#define DEBUG_LEVEL_FINE
#define DEBUG_LEVEL_TRACE
//#define DEBUG_LEVEL_DETAIL
#define DEBUG_LEVEL_LOG
#define DEBUG_LEVEL_WARN
#define DEBUG_LEVEL_ERROR
#define DEBUG_TOCONSOLE
//#define DEBUG_TOFILE
//#define DEBUG_TOHTML
//#define DEBUG_WATCH

using UnityEngine;
using System.Collections;
using System.IO;
using System;

public static class D
{
	private static StreamWriter 	m_writer;
	private static StreamWriter 	m_html;
	
	//private static DLogger			m_logger;
	
	private static string			m_logPath;
	private static string			m_logName;
	
	
	static D()
	{
		//Debug.Log("D");
		
		//GameObject _logger = GameObject.Find("DLogger");
		
		//if ( _logger == null ) {
		//	//Debug.Log("Adding ULogger GameObject");
		//	_logger = new GameObject();
		//	_logger.name = "DLogger";
		//	_logger.AddComponent("DLogger");
		//}
		
		//m_logger = _logger.GetComponent<DLogger>();
		
		//m_logPath = Application.dataPath + "\\";
		//m_logName = "dlogger";
		
		//if ( m_logger.LoggerPath != "" ) {
		//	m_logPath = m_logger.LoggerPath;
		//}
		
		//if ( m_logger.LoggerName != "" ) {
		//	m_logName = m_logger.LoggerName;
		//}
		
		m_logPath = DLogger.Instance.LoggerPath;
		m_logName = DLogger.Instance.LoggerName;
		
		//Debug.Log(m_logPath + m_logName);

		CreateLogFile();		
		CreateHtmlLogFile();
		
		Application.RegisterLogCallback( logCallback );
	}
	
	
	public static void logCallback( string log, string stackTrace, LogType type )
	{
		// error gets a stack trace
		if( type == LogType.Error )
		{
			System.Console.WriteLine( log );
			System.Console.WriteLine( stackTrace );
		}
		else
		{
			System.Console.WriteLine( log );
		}
	}
	
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_FINE" )]
	public static void Fine(object message, params object[] paramList )
	{
		Log(message, paramList);
	}
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_FINE" )]
	public static void Fine(LogCategory cat, object message, params object[] paramList )
	{
		Log(cat, message, paramList);
	}
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_TRACE" )]
	public static void Trace(object message, params object[] paramList )
	{
		Log(message, paramList);
	}
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_TRACE" )]
	public static void Trace(LogCategory cat, object message, params object[] paramList )
	{
		Log(cat, message, paramList);
	}
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_DETAIL" )]
	public static void Detail(object message, params object[] paramList )
	{
		Log(message, paramList);
	}
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_DETAIL" )]
	public static void Detail(LogCategory cat, object message, params object[] paramList )
	{
		Log(cat, message, paramList);
	}
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_LOG" )]
	public static void Log(object message, params object[] paramList )
	{
		if( paramList.Length > 0 ) {
			LogToConsole(string.Format(message as string, paramList));
			LogToFile(LogCategory.General, string.Format(message as string, paramList));
			LogToHtml(LogCategory.General, string.Format(message as string, paramList ) );
		}
		else {
			LogToConsole(message);
			LogToFile(LogCategory.General, message);
			LogToHtml(LogCategory.General, message );
		}
	}
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_LOG" )]
	public static void Log( LogCategory cat, object message, params object[] paramList )
	{
		if( paramList.Length > 0 ) {
			LogToConsole(string.Format(message as string, paramList));
			LogToFile(cat, string.Format(message as string, paramList));
			LogToHtml(cat, string.Format(message as string, paramList ) );
		}
		else {
			LogToConsole(message);
			LogToFile(cat, message);
			LogToHtml(cat, message);
		}
	}
	
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_WARN" )]
	public static void Warn(object message, params object[] paramList )
	{
		if( paramList.Length > 0 ) {
			LogWarnToConsole(string.Format(message as string, paramList));
			LogToFile(LogCategory.Warn, string.Format(message as string, paramList));
			LogToHtml(LogCategory.Warn, string.Format(message as string, paramList));
		}
		else {
			LogWarnToConsole(message);
			LogToFile(LogCategory.Warn, message);
			LogToHtml(LogCategory.Warn, message);
		}
	}
	
	[System.Diagnostics.Conditional( "DEBUG_LEVEL_ERROR" )]
	public static void Error(object message, params object[] paramList )
	{
		if( paramList.Length > 0 ) {
			LogErrorToConsole(string.Format(message as string, paramList));
			LogToFile(LogCategory.Error, string.Format(message as string, paramList));
			LogToHtml(LogCategory.Error, string.Format(message as string, paramList));
		}
		else {
			LogErrorToConsole(message);
			LogToFile(LogCategory.Error, message);
			LogToHtml(LogCategory.Error, message);
		}
	}
	
	[System.Diagnostics.Conditional("DEBUG_LEVEL_LOG"),System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void assert( bool condition )
	{
		assert( condition, string.Empty, true );
	}

	
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void assert( bool condition, string assertString )
	{
		assert( condition, assertString, false );
	}

	
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void assert( bool condition, string assertString, bool pauseOnFail )
	{
		if( !condition )
		{
			Debug.LogError( "assert failed! " + assertString );
			
			if( pauseOnFail )
				Debug.Break();
		}
	}
	
	[System.Diagnostics.Conditional( "DEBUG_WATCH" )]
	public static void WatchGroup(string group, object props) {
	}
	
	[System.Diagnostics.Conditional( "DEBUG_WATCH" )]
	public static void Watch(string key, object val) {
		DLogger.Instance.SetWatch(key,val);	
	}
	
	[System.Diagnostics.Conditional( "DEBUG_WATCH" )]
	public static void Watch(string group, string key, object val) {
		DLogger.Instance.SetWatch(group, key,val);	
	}
	
	[System.Diagnostics.Conditional( "DEBUG_WATCH" )]
	public static void ClearWatch(string name) {
		DLogger.Instance.ClearWatch(name);
	}
	
	
	[System.Diagnostics.Conditional("DEBUG_TOFILE"),System.Diagnostics.Conditional("UNITY_EDITOR")]
	private static void CreateLogFile() {
		Debug.Log(string.Format("- DLogger creating log file {0}{1}.log", m_logPath, m_logName));
		m_writer = new StreamWriter( m_logPath + m_logName + ".log", false, System.Text.Encoding.UTF8, 1 );
		m_writer.AutoFlush = false;
		LogToFile(LogCategory.General, "DLogger is active...");
	}

	[System.Diagnostics.Conditional("DEBUG_TOFILE"),System.Diagnostics.Conditional("UNITY_EDITOR")]
	private static void CloseLogFile() {
		Debug.LogWarning("- DLogger is closing log file");
		LogToFile(LogCategory.General, "DLogger is closing log file...");
		m_writer.Close();
	}
		
	[System.Diagnostics.Conditional("DEBUG_TOHTML"),System.Diagnostics.Conditional("UNITY_EDITOR")]
	private static void CreateHtmlLogFile() {
		Debug.Log(string.Format("- DLogger creating html log file {0}{1}.html", m_logPath, m_logName));
		m_html = new StreamWriter( m_logPath + m_logName + ".html", false );
		m_html.AutoFlush = true;
		m_html.WriteLine("<head>");
		m_html.WriteLine("<script language=\"javascript\" src=\"http://dev1.sdngames.com/dlstyle/dlogger.javascript\"></script>");
		m_html.WriteLine("<link rel=\"stylesheet\" type=\"text/css\" href=\"http://dev1.sdngames.com/dlstyle/dlogger.css\" />");
		m_html.WriteLine("</head>");
		LogToHtml(LogCategory.General, "DLogger is active...");
	}
	
	[System.Diagnostics.Conditional("DEBUG_TOHTML"),System.Diagnostics.Conditional("UNITY_EDITOR")]
	private static void CloseHtmlLogFile() {
		Debug.LogWarning("- DLogger is closing html log file");
		LogToHtml(LogCategory.General, "DLogger is closing html log file...");
		m_html.Close();
	}
	
	[System.Diagnostics.Conditional("DEBUG_TOCONSOLE")]
	private static void LogToConsole(object log) {
		Debug.Log(log);
	}
	
	[System.Diagnostics.Conditional("DEBUG_TOCONSOLE")]
	private static void LogWarnToConsole(object log) {
		Debug.LogWarning(log);
	}
	
	[System.Diagnostics.Conditional("DEBUG_TOCONSOLE")]
	private static void LogErrorToConsole(object log) {
		Debug.LogError(log);
	}
	
	[System.Diagnostics.Conditional("DEBUG_TOFILE"),System.Diagnostics.Conditional("UNITY_EDITOR")]
	private static void LogToFile(LogCategory cat, object log) {
		string _val = log as string;
		string _cat = Enum.GetName(typeof(LogCategory), cat);
		string _log = string.Format("{0,-25}| {1,-20}| {2}", System.DateTime.Now.ToString("MM/dd/yyy hh:mm:ss.fff"), _cat, _val);
		m_writer.WriteLine(_log);
	}

	[System.Diagnostics.Conditional("DEBUG_TOHTML"),System.Diagnostics.Conditional("UNITY_EDITOR")]
	private static void LogToHtml(LogCategory cat, object log) {
		string _cat = Enum.GetName(typeof(LogCategory), cat);
		m_html.Write("<p class=\"" + _cat + "\">");
		m_html.Write("<span class=\"Icon\"><img src=\"http://dev1.sdngames.com/dlstyle/{0}.png\" title=\"" + _cat + "\"></span><span class=\"Time\">{1}</span>", _cat.ToLower(), System.DateTime.Now.ToString("MM/dd/yyy hh:mm:ss.fff"));
		string _log = log as string;
		//string _log = string.Format(_val);
		m_html.Write(_log);
		m_html.Write("</p>");
	}
	
	public static void Quit() {
		CloseHtmlLogFile();
		CloseLogFile();
	}
	
}

[Flags] public enum LogCategory {
	
	General 		= 100,
	Warn			= 110,
	Error			= 120,
	System 			= 200,
	SystemInput		= 210,
	GUI 			= 300, 
	GUIEvent		= 310,
	AI 				= 400,
	Network 		= 600,
	NetworkEvent	= 610,
	Physics 		= 700, 
	Game			= 800,
	GameAudio		= 810,
	GameMusic		= 820,
	Camera 			= 900		
}
